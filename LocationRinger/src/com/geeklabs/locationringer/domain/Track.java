package com.geeklabs.locationringer.domain;

import java.io.Serializable;
import java.util.Date;

public class Track implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	public double latitude;
	public double longitude;
	public Date trackTime;
	private String address;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public Date getTrackTime() {
		return trackTime;
	}
	public void setTrackTime(Date date) {
		this.trackTime = date;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}