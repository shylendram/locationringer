package com.geeklabs.locationringer.util;
public final class RequestUrl {

	private RequestUrl() {}
	
	/*public static final String VALIDATE_TOKEN_REQ= "http://192.168.1.114:9999/user/signin";
	public static final String SIGNOUT_REQ = "http://192.168.1.114:9999/user/signout/{id}";
	public static final String SEND_TRACKS = "http://192.168.1.114:9999/user/updateLocation/{id}";
	public static final String GET_LOCATION = "http://192.168.1.114:9999/user/getLocation/{number}";*/
	
	public static final String VALIDATE_TOKEN_REQ= "http://locationringing.appspot.com/user/signin";
	public static final String SIGNOUT_REQ = "http://locationringing.appspot.com/user/signout/{id}";
	public static final String SEND_TRACKS = "http://locationringing.appspot.com/user/updateLocation/{id}";
	public static final String GET_LOCATION = "http://locationringing.appspot.com/user/getLocation/{number}";
}