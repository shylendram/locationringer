package com.geeklabs.locationringer.util;
public final class BroadcastEvent {

	private BroadcastEvent() {}
	
	public static final String START_TRACK_SERVICE = "com.geeklabs.locationringer.START_TRACK_SERVICE";
	public static final String RESUME_TRACK_SERVICE = "com.geeklabs.locationringer.RESUME_TRACK_SERVICE";
	public static final String STOP_TRACK_SERVICE = "com.geeklabs.locationringer.STOP_TRACK_SERVICE";
}