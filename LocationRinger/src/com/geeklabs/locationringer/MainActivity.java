package com.geeklabs.locationringer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.geeklabs.locationringer.oauth.AuthActivity;
import com.geeklabs.locationringer.preferences.AuthPreferences;
import com.geeklabs.locationringer.util.NetworkService;

public class MainActivity extends Activity {

	private EditText mblNumditText;
	private AuthPreferences authPreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		authPreferences = new AuthPreferences(getApplicationContext());

/*		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				 * Intent intent = new Intent(getApplicationContext(),
				 * LocationRingerIntentService.class);
				 * intent.putExtra("location", "Nizamabad");
				 * startService(intent);
				 
				TrackingServiceManager.startTracking(MainActivity.this);
			}
		});*/
		/*findViewById(R.id.button2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				 * Intent intent = new Intent(getApplicationContext(),
				 * LocationRingerIntentService.class); stopService(intent);
				 
				TrackingServiceManager.stopTracking(getApplicationContext());
			}
		});
*/		
			findViewById(R.id.sign_in_button).setOnClickListener(new OnClickListener() {
				@SuppressWarnings("null")
				@Override
				public void onClick(View v) {
					// Check for n/w connection
					if (!NetworkService.isNetWorkAvailable(MainActivity.this)) {
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
						// set title
						alertDialogBuilder.setTitle("Warning");
						alertDialogBuilder.setMessage("Check your network connection and try again.");
						// set dialog message
						alertDialogBuilder.setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
							}
						});
						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();

						// show it
						alertDialog.show();
					} else if (authPreferences.getUserId() > 0 && authPreferences.isUserSignedIn()) {
						Intent i = new Intent(getApplicationContext(), HomeActivity.class);
						startActivity(i);
					}else {
						mblNumditText = (EditText) findViewById(R.id.mbl_num_edittex);
						final String mblNum = mblNumditText.getText().toString();
						if (mblNum != null && !mblNum.isEmpty() && mblNum.length() == 10) {
							Intent i = new Intent(getApplicationContext(), AuthActivity.class);
							i.putExtra("mblNum", mblNum);
							startActivity(i);
						}else {
							Toast.makeText(getApplicationContext(), "plz enter proper mobile number", Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
			
		}
}
