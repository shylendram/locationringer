package com.geeklabs.locationringer.communication.get.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.locationringer.communication.AbstractHttpGetTask;
import com.geeklabs.locationringer.domain.Track;

public class GetTracksTask extends AbstractHttpGetTask {

	private Activity contextActivity;

	public GetTracksTask(int currentPage, Activity context, ProgressDialog getTracksProgressDialog) {
		super(getTracksProgressDialog, context);
		this.contextActivity = context;
	}

	@Override
	protected String getRequestUrl() {
		return null;
	}

	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message, Toast.LENGTH_SHORT).show();
				Log.i("Loading ...", message);
			}
		});

	}

	@SuppressLint("NewApi")
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		try {
			List<Track> tracks = new ArrayList<Track>();
			if (!jsonResponse.isEmpty()) {
				ObjectMapper mapper = new ObjectMapper();
				tracks = mapper.readValue(jsonResponse, new TypeReference<List<Track>>(){});
				Log.i("Tracks", String.valueOf(tracks));
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {			
			cancelDialog();
		}

	}

}
