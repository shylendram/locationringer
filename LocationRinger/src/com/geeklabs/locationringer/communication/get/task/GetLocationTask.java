package com.geeklabs.locationringer.communication.get.task;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.geeklabs.locationringer.Ringer;
import com.geeklabs.locationringer.communication.AbstractHttpGetTask;
import com.geeklabs.locationringer.domain.User;
import com.geeklabs.locationringer.service.LocationRingerIntentService;
import com.geeklabs.locationringer.util.RequestUrl;

public class GetLocationTask extends AbstractHttpGetTask {

	private String mblNumber;
	private Context context;

	public GetLocationTask(ProgressDialog progressDialog, Context context, String incomingNumber) {
		super(progressDialog, context);
		this.mblNumber = incomingNumber;
		this.context = context;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.GET_LOCATION.replace("{number}", mblNumber);
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show any message
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			User user = null;
			try {
				 user = mapper.readValue(jsonResponse, User.class);
				/* Intent intent = new Intent(context, CallRingerIntentService.class);
				 intent.putExtra("com.geeklabs.locationringer", user);
				 context.startActivity(intent);*/
				 if (user == null) {
					 RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, Ringer.uri);
				 }
				 
				 String ring = "you have a call from ";
				 
				 String speakTextTxt1;
				 HashMap<String, String> myHashRender = new HashMap<String, String>();
				 String tempDestFile;

				 // Get ctx from phone directory
				 String contactName = getContactName(context, user.getMobileNumber());
				 
				 if (contactName != null) {
					 
					speakTextTxt1 = ring + contactName + "  from  " + user.getAddress();
					myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, speakTextTxt1);

					String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
					File appTmpPath = new File(exStoragePath + "/LRinger");
					if (!appTmpPath.exists()) {
						appTmpPath.mkdirs();
					} else {
						File[] listFiles = appTmpPath.listFiles();
						for (File file : listFiles) {
							file.delete();
						}
					}
					
					String tempFilename = contactName + ".mp3";
					tempDestFile = appTmpPath.getAbsolutePath() + "/" + tempFilename;

					new MySpeech(speakTextTxt1, myHashRender, tempDestFile);	
						
					// show toast in middle of the screen
					Toast toast = Toast.makeText(context, ring + contactName + ", with number " + user.getMobileNumber() + " from "
							+ user.getAddress(), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					File newSoundFile = new File(tempDestFile);
					File file = new File(context.getExternalCacheDir(), tempFilename);
					if (!file.exists()) {

						ContentValues values = new ContentValues();
						values.put(MediaStore.MediaColumns.DATA, newSoundFile.getAbsolutePath());
						values.put(MediaStore.MediaColumns.TITLE, contactName);
						values.put(MediaStore.MediaColumns.SIZE, 2030);
						values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
						values.put(MediaStore.Audio.Media.ARTIST, "LRinger");
						values.put(MediaStore.Audio.Media.DURATION, newSoundFile.length()); //
						values.put(MediaStore.Audio.Media.IS_RINGTONE, true);

						Uri uri = MediaStore.Audio.Media.getContentUriForPath(newSoundFile.getAbsolutePath());
						Uri newUri = context.getContentResolver().insert(uri, values);
						if (newUri == null) {
							Toast.makeText(context, "new uri is null", Toast.LENGTH_SHORT).show();
						}
//						getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + newSoundFile + "\"", null);
						RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, newUri);
					}
				} else {
					Toast.makeText(context, "Contact not found for the number " + user.getMobileNumber(), Toast.LENGTH_LONG).show();
				}

			} catch (JsonParseException e) {
				e.printStackTrace();
				RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, Ringer.uri);
			} catch (JsonMappingException e) {
				e.printStackTrace();
				RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, Ringer.uri);
			} catch (IOException e) {
				e.printStackTrace();
				RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, Ringer.uri);
			}
		} 
	}
	
	public String getContactName(Context context, String number) {
		String name = "Unknown";
		// define the columns I want the query to return
		String[] projection = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID };
		// encode the phone number and build the filter URI
		Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
		// query time
		Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
			} else {
				Toast.makeText(context, "Contact name not found for the  " + number, Toast.LENGTH_LONG).show();
			}
			cursor.close();
		}
		return name;
	}
	
	class MySpeech implements OnInitListener {
		String speakTextTxt1;
		private TextToSpeech mTts;
		private HashMap<String, String> myHashRender;
		private String tempDestFile;

		public MySpeech(String tts, HashMap<String, String> myHashRender, String tempDestFile) {
			this.speakTextTxt1 = tts;
			this.myHashRender = myHashRender;
			this.tempDestFile = tempDestFile;
			mTts = new TextToSpeech(context, this);
		}

		@Override
		public void onInit(int status) {
			Log.v("onInit:", "on init called");
			if (status == TextToSpeech.SUCCESS) {
				int result = mTts.setLanguage(Locale.UK);
				if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
					Log.i("TTS", "This Language is not supported");
					Toast.makeText(context, "This Language is not supported", Toast.LENGTH_LONG).show();
				} else {
					mTts.synthesizeToFile(speakTextTxt1, myHashRender, tempDestFile);
				}
			} else {
				Log.e("TTS", "Initilization Failed!");
				Toast.makeText(context, "Initilization Failed!", Toast.LENGTH_LONG).show();
			}
		}
	}

}
