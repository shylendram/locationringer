package com.geeklabs.locationringer.communication.get.task;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.locationringer.MainActivity;
import com.geeklabs.locationringer.communication.AbstractHttpGetTask;
import com.geeklabs.locationringer.preferences.AuthPreferences;
import com.geeklabs.locationringer.response.ResponseStatus;
import com.geeklabs.locationringer.util.RequestUrl;
import com.geeklabs.locationringer.util.TrackingServiceManager;

public class SignoutTask extends AbstractHttpGetTask {
	private AuthPreferences authPreferences;
	private Activity contextActivity;

	public SignoutTask(Activity context, ProgressDialog signoutProgressDialog) {
		super(signoutProgressDialog, context);
		this.contextActivity = context;
		authPreferences = new AuthPreferences(context);
	}
	
	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message,
						Toast.LENGTH_SHORT).show();
				Log.i("FM Signout Processing", message);
			}
		});
	}
	
	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			ResponseStatus responseStatus = null;
			try {
				responseStatus = mapper.readValue(jsonResponse, ResponseStatus.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if ("success".equals(responseStatus.getStatus())) {
				// Stop tracking service
				TrackingServiceManager.stopTracking(contextActivity);
				
				// Request signout to Google server
				singoutUser();
				
				// Set user sign in status 
				authPreferences.setSignInStatus(false);

				// User id
				authPreferences.setUserId(0);
				
				//set user_uuid
				authPreferences.setUserUID("");
				
				Intent i = new Intent(contextActivity, MainActivity.class);
				contextActivity.startActivity(i);
				
				showMessageOnUI(" Your successfully logged out");
				cancelDialog();
		}
		
				} else {
			showMessageOnUI("Unable to reach server, try after sometime");
			cancelDialog();
		}
	}

	private void singoutUser() {
		AccountManager accountManager = AccountManager.get(contextActivity);
		accountManager.invalidateAuthToken("com.google", authPreferences.getAccessToken());
 
		authPreferences.setAccessToken(null);
	}
	
	@Override
	protected String getRequestUrl() {
		return RequestUrl.SIGNOUT_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}
}
