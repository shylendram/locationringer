package com.geeklabs.locationringer.communication.post.task;
import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.locationringer.HomeActivity;
import com.geeklabs.locationringer.MainActivity;
import com.geeklabs.locationringer.communication.AbstractHttpPostTask;
import com.geeklabs.locationringer.domain.User;
import com.geeklabs.locationringer.preferences.AuthPreferences;
import com.geeklabs.locationringer.response.ResponseStatus;
import com.geeklabs.locationringer.util.RequestUrl;
import com.geeklabs.locationringer.util.TrackingServiceManager;
public class RegisterUserTask extends AbstractHttpPostTask {
	private AuthPreferences authPreferences;
	private Activity contextActivity;
//	private String userUid;
	private String accessToken;
//	private ProgressDialog signingAPKProgressDialog;
	private User user;

	public RegisterUserTask(Activity context, ProgressDialog signingAPKProgressDialog, User user) {
		super(signingAPKProgressDialog, context);
		this.contextActivity =  context;
		authPreferences = new AuthPreferences(context);
//		userUid = UUID.randomUUID().toString();
		this.user = user;
	}
	
	@Override
	protected void showMessageOnUI(final String message) {
		contextActivity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(contextActivity, message, Toast.LENGTH_SHORT).show();
				Log.i("FM Request Processing", message);
			}
		});
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException { 
		
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			ResponseStatus responseStatus = null;
			try {
				responseStatus = mapper.readValue(jsonResponse.toString(), ResponseStatus.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String status = responseStatus.getStatus();
				
				// send first track
				if (responseStatus != null && status.equals("success")) {
					
					// Start tracking service
					TrackingServiceManager.startTracking(contextActivity);
					// Set user sign in status 
					authPreferences.setSignInStatus(true);
					authPreferences.setAccessToken(accessToken);
					
					// User id
					long userId = responseStatus.getId();
					authPreferences.setUserId(userId);
					
					//set user_uuid
//					authPreferences.setUserUID(userUid);
					
					//update preferences
					/*Preferences preferences = user.getPreferencesDto();
					if (preferences != null) {
					}*/
					
					
					Intent i = new Intent(contextActivity, HomeActivity.class);
					i.putExtra("image", responseStatus.getImageUrl());
					i.putExtra("userName", responseStatus.getUserName());
					contextActivity.startActivity(i);
					showMessageOnUI("success fully logged in");
				}else if (status.equals("mobile num")) {
					Intent i = new Intent(contextActivity, MainActivity.class);
					contextActivity.startActivity(i);
					
					showMessageOnUI("enter valid mobile num");
				} 
		} else {
			Intent i = new Intent(contextActivity, MainActivity.class);
			contextActivity.startActivity(i);
			
			showMessageOnUI("plz try later");
		}
		
	}
		
		
		 /*else if("UuidInActive".equals(status)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(contextActivity);
			builder.setTitle("User Login Issue");
			builder.setMessage("With this account, application is running in another device. Are you sure you want to singin again with this device");
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(contextActivity, MainActivity.class);
					contextActivity.startActivity(i);
				}
			});
			
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					user.setResignIn("true");
					
					user.setDevice(android.os.Build.DEVICE); 
					user.setModel(android.os.Build.MODEL);
					user.setProduct(android.os.Build.PRODUCT);
					user.setUser(android.os.Build.USER);
					user.setSdkInt(android.os.Build.VERSION.SDK_INT);
					
					// Again send request to sign in
					RegisterUserTask registerUserTask = new RegisterUserTask(contextActivity, signingAPKProgressDialog, accessToken, user, null);
					registerUserTask.execute();
				}
			});
			
			builder.show();*/

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		String writeValueAsString  = null;
			try {
				writeValueAsString = mapper.writeValueAsString(user);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return writeValueAsString;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.VALIDATE_TOKEN_REQ;
	}
}