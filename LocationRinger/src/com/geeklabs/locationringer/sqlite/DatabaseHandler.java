package com.geeklabs.locationringer.sqlite;

import java.util.ArrayList;
import java.util.List;

import com.geeklabs.locationringer.domain.Track;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	 
	    public DatabaseHandler(Context context) {
	        super(context, "tracks", null, 1);
	    }
	 
	    @Override
	    public void onCreate(SQLiteDatabase db) {
	        String CREATE_TRACKS_TABLE = "CREATE TABLE tracks (" +
	                "time DATETIME DEFAULT CURRENT_TIMESTAMP," +
	                "latitude DOUBLE," +
	                "longitude DOUBLE)";
	        db.execSQL(CREATE_TRACKS_TABLE);
	         
	    }
	 
	    @Override
	    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        db.execSQL("DROP TABLE IF EXISTS tracks");
	        onCreate(db);
	    }
	     
	    public void insertRow(Double latitude, Double longitude, String time) {
	        SQLiteDatabase db = this.getWritableDatabase();
	        ContentValues values = new ContentValues();
	        values.put("latitude", latitude);
	        values.put("longitude", longitude);
	        values.put("time", time);
	         
	        db.insert("tracks", null, values);
	        Log.i("Track saved", latitude + "," + longitude + "," + time);
	        db.close();
	    }
	 
	    public List<Track> getAllTracks() {
	        List<Track> trackList = new ArrayList<Track>();
	        String selectQuery = "SELECT latitude, longitude, time FROM tracks";
	  
	        SQLiteDatabase db = this.getWritableDatabase();
	        Cursor cursor = db.rawQuery(selectQuery, null);
	  
	        if (cursor.moveToFirst()) {
	            do {
	                Track track = new Track();
	                track.latitude = Double.parseDouble(cursor.getString(0));
	                track.longitude = Double.parseDouble(cursor.getString(1));
	                trackList.add(track);
	            } while (cursor.moveToNext());
	        }
	        cursor.close();
	        db.close();
	        return trackList;
	    }
	}