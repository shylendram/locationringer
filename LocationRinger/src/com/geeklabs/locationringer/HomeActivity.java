/**
 * 
 */
package com.geeklabs.locationringer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.geeklabs.locationringer.communication.ImageLoadTask;
import com.geeklabs.locationringer.communication.get.task.SignoutTask;

/**
 * @author ayyappa
 * 
 */
public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		Intent intent = getIntent();
		/*
		 * StrictMode.ThreadPolicy policy = new
		 * StrictMode.ThreadPolicy.Builder().permitAll().build();
		 * StrictMode.setThreadPolicy(policy);
		 */
		String photo_url_str = intent.getStringExtra("image");
		String userName = intent.getStringExtra("userName");
		ImageView profile_photo = (ImageView) findViewById(R.id.imageView1);
		TextView textView = (TextView) findViewById(R.id.nametextview);
		textView.setText(userName);
		new ImageLoadTask(photo_url_str, profile_photo).execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);// Menu Resource, Menu
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.signout:
			signoutUser();
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void signoutUser() {
		final ProgressDialog signoutAPKProgressDialog = new ProgressDialog(HomeActivity.this);
		signoutAPKProgressDialog.setMessage("User signout is in progress, please wait...");
		Log.i("User signout", "User signout is in progress");
		signoutAPKProgressDialog.setCancelable(false);

		// Job required for validation, registration will be taken care in
		SignoutTask signoutTask = new SignoutTask(HomeActivity.this, signoutAPKProgressDialog);
		signoutTask.execute();
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}
}
