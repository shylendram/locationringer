package com.geeklabs.locationringer.service;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.geeklabs.locationringer.domain.User;

public class CallRingerIntentService extends IntentService {

	private static final String TAG = "com.gl.Lringer";
	private String contactName;
	private static String ring = "you have a call from ";
//	private String location;
	private Toast toast = null;
	private TextToSpeech mTts;
	private PhoneCallListener phoneListener = new PhoneCallListener();
	private User user;

	@Override
	protected void onHandleIntent(Intent workIntent) {
		Log.i(TAG, "Intent Service started");

		//user = (User)workIntent.getSerializableExtra("com.geeklabs.locationringer");
		user = new User();
		user.setAddress("Habsiguda");

		TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

	}

	public CallRingerIntentService() {
		super("CallRingerIntentService");
	}

	class PhoneCallListener extends PhoneStateListener {

		private boolean isPhoneCalling = false;
		private String LOG_TAG = "Result log:";
		private String speakTextTxt1;
		private HashMap<String, String> myHashRender = new HashMap<String, String>();
		private String tempDestFile;

		@SuppressLint("SdCardPath")
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			if (TelephonyManager.CALL_STATE_RINGING == state) {
				// get contact name of incoming number
				contactName = getContactName(getApplicationContext(), incomingNumber);

				if (contactName != null) {
					// when is phone ringing then get incoming number
					Log.i(LOG_TAG, "Ringing Number is: " + incomingNumber);
					Log.i(LOG_TAG, "Contact Name is: " + contactName);

					speakTextTxt1 = CallRingerIntentService.ring + contactName + "  from  " + user.getAddress();
					myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, speakTextTxt1);

					String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
					File appTmpPath = new File(exStoragePath + "/LRinger");
					if (!appTmpPath.exists()) {
						appTmpPath.mkdirs();
					}
					String tempFilename = contactName + ".mp3";
					tempDestFile = appTmpPath.getAbsolutePath() + "/" + tempFilename;
					new MySpeech(speakTextTxt1);

					// show toast in middle of the screen
					toast = Toast.makeText(getApplicationContext(), ring + contactName + ", with number " + incomingNumber + " from "
							+ user.getAddress(), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					File newSoundFile = new File(tempDestFile);
					File file = new File(getExternalCacheDir(), tempFilename);
					if (!file.exists()) {

						ContentValues values = new ContentValues();
						values.put(MediaStore.MediaColumns.DATA, newSoundFile.getAbsolutePath());
						values.put(MediaStore.MediaColumns.TITLE, contactName);
						values.put(MediaStore.MediaColumns.SIZE, 2030);
						values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
						values.put(MediaStore.Audio.Media.ARTIST, "LRinger");
						values.put(MediaStore.Audio.Media.DURATION, newSoundFile.length()); //
						values.put(MediaStore.Audio.Media.IS_RINGTONE, true);

						Uri uri = MediaStore.Audio.Media.getContentUriForPath(newSoundFile.getAbsolutePath());
						Uri newUri = getContentResolver().insert(uri, values);
						if (newUri == null) {
							Toast.makeText(getApplicationContext(), "new uri is null", Toast.LENGTH_SHORT).show();
						}
//						getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + newSoundFile + "\"", null);
						RingtoneManager.setActualDefaultRingtoneUri(CallRingerIntentService.this, RingtoneManager.TYPE_RINGTONE, newUri);
						Log.i(LOG_TAG, "Default ring tone changed to " + tempFilename + " successfully");
					}
				} else {
					Toast.makeText(getApplicationContext(), "Contact not found for the number " + incomingNumber, Toast.LENGTH_LONG).show();
				}
			}

			if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
				// when call is in active state
				Log.i(LOG_TAG, "Call is lifted right now");
				isPhoneCalling = true;
			}

			if (TelephonyManager.CALL_STATE_IDLE == state) {
				// run when class initial and phone call ended
				Log.i(LOG_TAG, "Call is in IDLE state");
				if (isPhoneCalling) {
					if (toast != null) {
						toast.cancel();
					}
					Handler handler = new Handler();
					/*
					 * Put in delay because call log is not updated immediately
					 * when state changed. The dialler takes a little bit of
					 * time to write to it 500ms seems to be enough
					 */

					handler.postDelayed(new Runnable() {

						@Override
						public void run() {
							// get start of cursor
							Log.i("When call lifted and ended", "call lifted and ended");
							toast.cancel();
						}
					}, 500);

					isPhoneCalling = false;
				}

			}
		}

		class MySpeech implements OnInitListener {
			String tts;

			public MySpeech(String tts) {
				this.tts = tts;
				mTts = new TextToSpeech(CallRingerIntentService.this, this);
			}

			@Override
			public void onInit(int status) {
				Log.v("onInit:", "on init called");
				if (status == TextToSpeech.SUCCESS) {
					int result = mTts.setLanguage(Locale.UK);
					if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
						Log.i("TTS", "This Language is not supported");
						Toast.makeText(getApplicationContext(), "This Language is not supported", Toast.LENGTH_LONG).show();
					} else {
						mTts.synthesizeToFile(speakTextTxt1, myHashRender, tempDestFile);
					}
				} else {
					Log.e("TTS", "Initilization Failed!");
					Toast.makeText(getApplicationContext(), "Initilization Failed!", Toast.LENGTH_LONG).show();
				}
			}
		}

		public String getContactName(Context context, String number) {
			String name = null;
			// define the columns I want the query to return
			String[] projection = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID };
			// encode the phone number and build the filter URI
			Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
			// query time
			Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
					Log.v(TAG, "Contact Found " + number);
					Log.v(TAG, "Contact Name  = " + name);
				} else {
					Log.v(TAG, "Contact Name Not Found @ " + number);
					Toast.makeText(getApplicationContext(), "Contact name not found for the  " + number, Toast.LENGTH_LONG).show();
				}
				cursor.close();
			}
			return name;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (toast != null) {
			toast.cancel();
		}
		// Close the Text to Speech Library
		if (mTts != null) {
			mTts.stop();
			mTts.shutdown();
			Log.d(TAG, "TTS Destroyed");
		}
	}
}