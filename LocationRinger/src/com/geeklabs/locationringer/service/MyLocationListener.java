package com.geeklabs.locationringer.service;

import java.util.Calendar;

import com.geeklabs.locationringer.sqlite.DatabaseHandler;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

public class MyLocationListener extends Service implements LocationListener {

	private Context _context;
	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // meters
	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 30 * 1; // minute
	// Declaring a Location Manager
	protected LocationManager _locationManager;

	public boolean _isGPSEnabled = false;
	public boolean _isNetworkEnabled = false;
	public boolean _canGetLocation = false;
	public Location _location;
	public double _latitude;
	public double _longitude;

	@Override
	public void onLocationChanged(Location location) {
		this._location = location;
 		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
		DatabaseHandler databaseHandler = new DatabaseHandler(this);
//		databaseHandler.insertRow(latitude, longitude, mydate);
		// TODO send tracks to server
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	public Location getCurrentLocation(Context context) {
		this._context = context;
		try {
			_locationManager = (LocationManager) _context.getSystemService(LOCATION_SERVICE);
			_isGPSEnabled = _locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			_isNetworkEnabled = _locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			if (!_isGPSEnabled && !_isNetworkEnabled) {
				// no network provider is enabled
				showSettingsAlert("Alert", "Set your parameters");
			} else {
				this._canGetLocation = true;
				if (_isNetworkEnabled) {
					_locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network");
					if (_locationManager != null) {
						_location = _locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (_location != null) {
							_latitude = _location.getLatitude();
							_longitude = _location.getLongitude();
						}
					}
				}
				// if GPS Enabled get lat/long using GPS Services
				if (_isGPSEnabled) {
					if (_location == null) {
						_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						Log.d("GPS Enabled", "GPS Enabled");
						if (_locationManager != null) {
							_location = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (_location != null) {
								_latitude = _location.getLatitude();
								_longitude = _location.getLongitude();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// do nothing
		}
		return _location;
	}

	public void showSettingsAlert(String title, String msg) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(_context);
		alertDialog.setTitle(title);
		alertDialog.setMessage(msg);
		// Settings button
		alertDialog.setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				_context.startActivity(intent);
			}
		});
		// cancel button
		alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		// show
		alertDialog.show();
	}

	public boolean canGetLocation() {
		return this._canGetLocation;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
