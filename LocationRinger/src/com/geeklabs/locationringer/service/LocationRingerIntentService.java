package com.geeklabs.locationringer.service;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.speech.tts.TextToSpeech;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.geeklabs.locationringer.Ringer;
import com.geeklabs.locationringer.communication.get.task.GetLocationTask;

public class LocationRingerIntentService extends IntentService {

	private static final String TAG = "com.gl.Lringer";
/*	private String contactName;
	private static String ring = "you have a call from ";
	private String location;
	private Toast toast = null;
	private TextToSpeech mTts;
*/	private PhoneCallListener phoneListener = new PhoneCallListener();

	@Override
	protected void onHandleIntent(Intent workIntent) {
		Log.i(TAG, "Intent Service started");
//		location = workIntent.getStringExtra("location");
		TelephonyManager telephonyManager = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneListener,
				PhoneStateListener.LISTEN_CALL_STATE);
	}

	public LocationRingerIntentService() {
		super("LocationRingerIntentService");
	}

	class PhoneCallListener extends PhoneStateListener {

	/*	private boolean isPhoneCalling = false;
		private String LOG_TAG = "Result log:";
		private String speakTextTxt1;
		private HashMap<String, String> myHashRender = new HashMap<String, String>();
		private String tempDestFile;
*/
		@SuppressLint("SdCardPath")
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			if (TelephonyManager.CALL_STATE_RINGING == state) {

				// call async task get location by mobile num
				// create progress dialog
				GetLocationTask getLocationTask = new GetLocationTask(null, getApplicationContext(), incomingNumber);
				getLocationTask.execute();

			} 
			if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
				RingtoneManager.setActualDefaultRingtoneUri(LocationRingerIntentService.this, RingtoneManager.TYPE_RINGTONE, Ringer.uri);
			}
		}
	}
}