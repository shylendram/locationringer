package com.geeklabs.locationringer.service.receiver;

import java.util.Calendar;

import com.geeklabs.locationringer.service.TrackingService;
import com.geeklabs.locationringer.util.TrackingServiceManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class StartTrackingBroadcastReceiver extends BroadcastReceiver {

	 @Override
	   public void onReceive(Context context, Intent intent) {
		 // After signin we start track with event 'START_TRACK_SERVICE', allow it , else
		 // in any other case we should make sure user sign in status and track status
		 /*BroadcastEvent.START_TRACK_SERVICE.equals(intentAction) || 
		 String intentAction = "";
		 if (intent != null) {
			 intentAction =  intent.getAction();
		 }
				 BroadcastEvent.RESUME_TRACK_SERVICE.equals(intentAction) && */
		 Log.i("FM: start track System event type", intent.getAction());
		 if (TrackingServiceManager.isUserLoggedIn(context)) {
			 Intent i = new Intent(context, TrackingService.class);
			 AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			 
			 PendingIntent pIntent = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
			 
			 Calendar cal = Calendar.getInstance();
	         cal.add(Calendar.SECOND, 10); // 10 sec
	         
	         // Repeat for every 60 seconds
			 alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 60*1000, pIntent);
			 
			 TrackingServiceManager.setTrackingStatusToRunnung(context);
		 }
	 }
}
