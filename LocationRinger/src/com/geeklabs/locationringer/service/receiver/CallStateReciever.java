package com.geeklabs.locationringer.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.widget.Toast;

import com.geeklabs.locationringer.service.MyLocationListener;
import com.geeklabs.locationringer.service.LocationRingerIntentService;

public class CallStateReciever extends BroadcastReceiver {
	private double latitude;
	private double longitude;

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Intent intentService = new Intent(context, LocationRingerIntentService.class);
		
		MyLocationListener myLocationListener = new MyLocationListener();
		myLocationListener.getCurrentLocation(context);
		
		if (myLocationListener.canGetLocation()) {
			Location currentLocation = myLocationListener._location;
			if (currentLocation != null) {
				latitude = currentLocation.getLatitude();
				longitude = currentLocation.getLongitude();
			}
			//TODO get location name using geo coding
		}
		
		//Toast.makeText(context, "You are at" + latitude + "," + longitude, Toast.LENGTH_SHORT).show();
		
		intentService.putExtra("location", "Nizamabad");
		context.startService(intentService);
	}
}
