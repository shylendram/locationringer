package com.geeklabs.locationringer.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.geeklabs.locationringer.communication.AbstractHttpPostTask;
import com.geeklabs.locationringer.communication.post.task.SendMyLocationTask;
import com.geeklabs.locationringer.domain.Track;
import com.geeklabs.locationringer.util.LocationServices;
import com.geeklabs.locationringer.util.NetworkService;
import com.geeklabs.locationringer.util.TrackingServiceManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class TrackingService extends Service implements LocationListener, ConnectionCallbacks, OnConnectionFailedListener {
	private static final String LOG_TAG = "TrackingService";
	// A request to connect to Location Services
	private LocationRequest mLocationRequest;
	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	private Location location;

	private double currentLat;
	private double currentLng;

	private double lastTrackedLat;
	private double lastTrackedLng;
	private Track track;
	private final IBinder mBinder = new UserLocationBinder();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!TrackingServiceManager.canContinue(getApplicationContext())) {
			Log.w(LOG_TAG, "Trying to start service, but no user logged in the app");
			return Service.START_NOT_STICKY; // Don't do anything as user not
												// signed in
		}

		Log.i(LOG_TAG, "Tracking is called");

		// Create Location req and Location client
		setUpLocationClientIfNeeded();
		
		// Connect the client.
		if (!mLocationClient.isConnected()) {
			mLocationClient.connect();
		}

		// Call send tracks to server service
		sendTracks();
		
		// saveTracks
//		saveTracks();

		return Service.START_NOT_STICKY;
	}

	private void sendTracks() {
		
		track = new Track();
		//set currentLat, currentLng, date to Track Obj
		if (currentLat ==0.0 && currentLng == 0.0) { // If location not found hardcode
			currentLat = 17.4192;
			currentLng = 78.5414;
		}
		track.setLatitude(currentLat);
		track.setLongitude(currentLng); 
//		track.setTrackTime(date);
				
		if (track != null && track.getLatitude() > 0 && track.getLongitude() > 0) { // TODO - hard coded to >1
			// Call send trackings to server service
			/*Intent sendTrackService = new Intent(this, SendTrackService.class);
			sendTrackService.putExtra("com.geeklabs.locationringer.track", track);
			this.startService(sendTrackService);*/
			
			if (NetworkService.isNetWorkAvailable(getApplicationContext())) {
				AbstractHttpPostTask sendTrackRequest = new SendMyLocationTask(getApplicationContext(), null, track);
				sendTrackRequest.execute();
			}
		}
	}

	private void setUpLocationClientIfNeeded() {
		createLocReq();
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}

	}

	private void createLocReq() {

		if (mLocationRequest == null) {
			// Create a new global location parameters object
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest.setInterval(LocationServices.UPDATE_INTERVAL_IN_MILLISECONDS);
			// Use high accuracy
			mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			// Set the interval ceiling to one minute
			mLocationRequest.setFastestInterval(LocationServices.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		}

	}


	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// If the client is connected
		if (mLocationClient != null && mLocationClient.isConnected()) {
			/*
			 * Remove location updates for a listener. The current Activity is
			 * the listener, so the argument is "this".
			 */
			stopPeriodicUpdates();
			mLocationClient.disconnect();
		}
	}
	
	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			this.location = location;
			updateLocation(location);
		}

	}

	public Location getLocation() {
		return location;
	}

	public void updateLocation(Location location) {
		if (lastTrackedLat == 0.0) {
			lastTrackedLat = location.getLatitude();
		}
		if (lastTrackedLng == 0.0) {
			lastTrackedLng = location.getLongitude();
		}

		currentLat = location.getLatitude();
		currentLng = location.getLongitude();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}
	public class UserLocationBinder extends Binder {
	    public TrackingService getService() {
	        return TrackingService.this;
	      }
	}
	
	/**
	 * In response to a request to stop updates, send a request to Location
	 * Services
	 */
	private void stopPeriodicUpdates() {
		mLocationClient.removeLocationUpdates(this);;
	}

	/**
	 * In response to a request to start updates, send a request to Location
	 * Services
	 */
	private void startPeriodicUpdates() {
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}
	
	@Override
	public void onConnected(Bundle arg0) {
		startPeriodicUpdates();
		Location location = mLocationClient.getLastLocation();
		if (location != null) {
			onLocationChanged(location);
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

}
